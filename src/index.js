import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.scss";
import { Provider } from "react-redux";
import store from "./redux/redux-store";
import AlertState from "./context/alert/AlertState";
// import FirebaseState from "./context/firebase/FirebaseState";

ReactDOM.render(
  // <FirebaseState>

  <Provider store={store}>
    <AlertState>
      <App />
    </AlertState>
  </Provider>,

  // </FirebaseState>
  document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

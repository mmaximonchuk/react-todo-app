import React, { useState } from "react";

const WeatherCityRequest = ({ requestNewCity, toggleCityRequestShown, handleStateMobileMenu}) => {
  const [searchValue, setSearchValue] = useState("");
  const handleInputChange = (e) => {
    setSearchValue(e.target.value);
  };
  // console.log(errorMsg);

  const searchFormSubmitHandler = (e) => {
    e.preventDefault();
    let formattedSearchValue = searchValue.toLocaleLowerCase();
    requestNewCity(formattedSearchValue);
    handleStateMobileMenu(false);
    toggleCityRequestShown(false);
  };
  return (
    <div className="city-request__container">
      <form className="search-form" onSubmit={searchFormSubmitHandler}>
        <input
          className="search-form__field"
          value={searchValue}
          onChange={handleInputChange}
          placeholder={"Enter city name..."}
          type="text"
        />
        <button className="search-form__btn" type="submit">
          Search
        </button>
      </form>
    </div>
  );
};

export default WeatherCityRequest;

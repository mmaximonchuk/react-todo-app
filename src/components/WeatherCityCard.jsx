import React from "react";
import cloudyDay from "../images/weather-app/cloudy-day.svg";
import iconClose from "../images/weather-app/close.svg";
import cloudyNight from "../images/weather-app/cloudy-night.svg";
import clearDay from "../images/weather-app/day.svg";
import drizzle from "../images/weather-app/drizzle.svg";
import rainy from "../images/weather-app/rainy.svg";
import snow from "../images/weather-app/snowy.svg";
import thunder from "../images/weather-app/thunder.svg";

const WeatherCityCard = ({
  id,
  removeCity,
  requestDetails,
  weatherData,
  toggleDetailsShown,
  arrayOfCurrentCityNames,
  arrayOfCurrentCityIds,
}) => {

  const showDetails = () => {
    toggleDetailsShown(true);
    requestDetails(weatherData.name);
  };
  let img;
  switch (weatherData.weather[0].main) {
    case "Clouds":
      img = cloudyNight;
      break;

    case "Thunderstorm":
      img = thunder;
      break;

    case "Drizzle":
      img = drizzle;
      break;

    case "Rain":
      img = rainy;
      break;

    case "Snow":
      img = snow;
      break;

    case "Clear":
      img = clearDay;
      break;

    default:
      img = clearDay;
      break;
  }
  return (
    <div className="city-wrapper">
      {arrayOfCurrentCityIds.indexOf(weatherData.id) === 0 ? null : <button className="city-wrapper__remove-btn" onClick={() => removeCity(id, weatherData.name.toLowerCase())}>
        <img src={iconClose} alt={'Close'}/>
      </button>}
      <div className="city" onClick={() => showDetails()}>
        <h2 className="city__title">{weatherData.name}</h2>
        <div className="city__forecast">
          <img src={img} alt="" />
          <span className="temperature">
            {Math.floor(weatherData.main.temp)}°C
          </span>
        </div>
        <span className="city__weather-type">
          {weatherData.weather[0].main}
        </span>
        <span className="city__feels-like">
          Feels like: {Math.floor(weatherData.main.feels_like)}°C
        </span>
      </div>
    </div>
  );
};

export default WeatherCityCard;

import React, {useState} from "react";
import imgBasket from ".././images/shopping-cart.svg";
const Good = ({id, addToBusket, image, description, title, price }) => {

const chooseGoodHandler = (id) => addToBusket(id);

  return (
    <div className="card">
      <img src={image} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <h5 className="card-text">
        <span className="item__price">
        {price} $
        </span>
        </h5>
        <div className="card__btns">
          <button onClick={() => alert('Temporary doesn`t work')} className="btn btn-primary">Buy now</button>
          <button onClick={() => chooseGoodHandler(id)} className="btn btn-secondary">
            Add to <img className="shopping-cart" src={imgBasket} alt="" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Good;

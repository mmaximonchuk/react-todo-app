import React from "react";
import { NavLink } from "react-router-dom";
import imgCart from '../images/shopping-cart.svg';
const Header = ({ setShowModalCart}) => {
  
  return (
    <nav className="navbar navbar-dark navbar-expand-lg bg-primary">
      <div className="navbar-brand">LazyWeather</div>
      <div className="navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          {/* <li className="nav-item">
            <NavLink exact to={"/"} className="nav-link">
              Home
            </NavLink>
          </li> */}
          {/* <li className="nav-item">
            <NavLink exact to={"/"} className="nav-link">
              Weather Forecast
            </NavLink>
          </li> */}
          {/* <li className="nav-item">
            <NavLink exact to={"/shop"} className="nav-link">
              Shop
            </NavLink>
          </li> */}
          {/* <li className="nav-item">
            <button onClick={() => setShowModalCart(prev => !prev)} className="btn navbar__cart nav-link">
              <img className="navbar__cart-img" src={imgCart} alt=""/>
            </button>
          </li> */}
        </ul>
      </div>
    </nav>
  );
};

export default Header;

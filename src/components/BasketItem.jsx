import React, { useState } from "react";
import imgBasket from "../images/garlic.jpg";

const BasketItem = ({
  id,
  image,
  title,
  totalPrice,
  price,
  removeGoodFromBasket,
  setQuantityOfGood,
  quantity,
}) => {
//   const handleChangeQuantity = (goodId, type, value) => {
//       setGoodQuantity(goodId, type, value);
//  };
 
  return (
    <div className="basket__item">
      <img src={image || imgBasket} className="item__img" alt="" />
      <div className="item__body">
        <p className="item__title">{title || "Title"}</p>
        <button
          onClick={() => setQuantityOfGood(id, quantity - 1)}
          className="btn-decrement"
        >
          -
        </button>
        {/* <input value={quantity} type="number" /> */}
        <span className="goodQuantity">{quantity}</span>
        <button
          onClick={() => setQuantityOfGood(id, quantity + 1)}
          className="btn-increment"
        >
          +
        </button>
        <div className="item__price">
          Total: {quantity * price || "100"}$
        </div>
      </div>
      <button onClick={() => removeGoodFromBasket(id)} className="btn-remove">
        &times;
      </button>
    </div>
  );
};

export default BasketItem;

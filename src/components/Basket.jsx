import React, { useState } from "react";
import BasketItem from "./BasketItem";
const Basket = ({ basketItems, setCulcTotalSum, removeGoodFromBasket, setQuantityOfGood, culcTotalSum }) => {
 setCulcTotalSum(basketItems);
  
  return (
    <div className="basket-container">
      {basketItems.length ? (
        <>
          {basketItems.map((item) => (
            <BasketItem
              key={item.id}
              basketItems={basketItems}
              removeGoodFromBasket={removeGoodFromBasket}
              setQuantityOfGood={setQuantityOfGood}
              {...item}
            />
          ))}
          <p className="fs-1">Total price: {culcTotalSum}$</p>
          <button type="button" className="btn btn-dark">
            Buy
          </button>
        </>
      ) : (
        <p className="notice">Your shopping cart is empty. Have fun to make purchase :)</p>
      )}
    </div>
  );
};

export default Basket;

import React, { useEffect } from "react";
import Slider from "react-slick";
import cloudyDay from "../images/weather-app/cloudy-day.svg";
import cloudyNight from "../images/weather-app/cloudy-night.svg";
import clearDay from "../images/weather-app/day.svg";
import drizzle from "../images/weather-app/drizzle.svg";
import rainy from "../images/weather-app/rainy.svg";
import snow from "../images/weather-app/snowy.svg";
import thunder from "../images/weather-app/thunder.svg";
import WeatherLoader from "./WeatherLoader";

const WeatherCityDetails = ({
  cityDetails,
  daysLasted,
  city,
}) => {

  let datesDataArray;
  if (daysLasted) {
    datesDataArray = daysLasted.map((item) =>
      Number.parseInt(new Date(item).toLocaleString("ru", { day: "2-digit" })),
    );
    datesDataArray = [...new Set(datesDataArray)];
  }

  const settings = {
    dots: false,
    arrows: false,
    infinite: false,
    speed: 800,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 475,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const year = new Date().getFullYear();
  const month = new Date().getMonth();
  const day = new Date().getDay();

  return (
      <div className="container">
        {!cityDetails ? (
          <WeatherLoader />
        ) : (
          <>
            <h2 className="city-modal__city-name">{city}</h2>
            {datesDataArray.map((currenData, index) => {
              return (
                <React.Fragment key={index + 1}>
                  <h3 className="city-details__date">
                    {new Date(year, month, day + index).toLocaleString("en", {
                      weekday: "long",
                      day: "2-digit",
                      month: "long",
                    })}
                  </h3>
                  <Slider {...settings} className="c-slider">
                    {cityDetails
                      .filter(
                        (item) =>
                          Number.parseInt(
                            new Date(item.dt_txt).toLocaleString("en", {
                              day: "2-digit",
                            }),
                          ) === datesDataArray[index],
                      )
                      .map((card) => {
                        let img;
                        switch (card.weather[0].main) {
                          case "Clouds":
                            img = cloudyNight;
                            break;

                          case "Thunderstorm":
                            img = thunder;
                            break;

                          case "Drizzle":
                            img = drizzle;
                            break;

                          case "Rain":
                            img = rainy;
                            break;

                          case "Snow":
                            img = snow;
                            break;

                          case "Clear":
                            img = clearDay;
                            break;

                          default:
                            img = clearDay;
                            break;
                        }
                        return (
                          <div className="c-slider__item" key={card.dt}>
                            <div className="city">
                              <h2 className="city__title">{`${new Date(card.dt_txt).toLocaleString("ru", {hour: '2-digit'})}:00`}</h2>
                              <div className="city__forecast city__forecast--primary">
                                <img src={img} alt="img" />
                                <span className="temperature">
                                  {Math.round(card.main.temp)}°C
                                </span>
                              </div>
                              <span className="city__weather-type">
                                {card.weather[0].description}
                              </span>
                              <span className="city__feels-like">
                                Feels like:{" "}
                                {Math.round(card.main.feels_like)}
                                °C
                              </span>
                            </div>
                          </div>
                        );
                      })}
                  </Slider>
                </React.Fragment>
              );
            })}
          </>
        )}
      </div>
  );
};

export default WeatherCityDetails;

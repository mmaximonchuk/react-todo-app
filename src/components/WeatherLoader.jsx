import React from 'react';

const WeatherLoader = ({className}) => {
    return (
        <div className={`lds-ellipsis ${className}`} ><div></div><div></div><div></div><div></div></div>
    );
};

export default WeatherLoader;
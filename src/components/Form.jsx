import React, {useState, useContext} from 'react';
import { AlertContext } from '../context/alert/alertContext';
import { FirebaseContext } from '../context/firebase/firebaseContext';

const Form = () => {
    const [value, setValue] = useState('');
    const alert = useContext(AlertContext);
    const firebase = useContext(FirebaseContext)

    const submitHandler = (e) => {
        e.preventDefault();

        if(value.trim()) {
            firebase.addNote(value.trim()).then(( ) => {
                alert.show('New note was created', 'alert-success')
            }).catch(() => {
                alert.show('Oops... Something went wrong(', 'alert-danger')
            })
            setValue('');
        } else {
            alert.show('Enter note\'s title!')
        }
    }
    return (
        <form onSubmit={submitHandler}>
            <div className="form-group">
                <input value={value} onChange={e => setValue(e.target.value)} type="text" className="form-control" placeholder="Enter a note's title..."/>
            </div>
        </form>
    );
};

export default Form;
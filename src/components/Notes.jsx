import React from "react";
import {TransitionGroup, CSSTransition} from 'react-transition-group';

const Notes = ({ notes, onRemove }) => (
    <TransitionGroup component="ul" className="list-group">
      {notes.map((note) => (
          <CSSTransition
            key={note.id}
            classNames={'note'}
            timeout={800}
          >
          <li className="list-group-item note">
            <div className="note-wrapper">
              <strong>{note.title}</strong>
              <small>{note.date}</small>
            </div>
            <button onClick={() => onRemove(note.id)} type="button" className="btn btn-sm btn-outline-danger">
              &times;
            </button>
          </li>
          </CSSTransition>
        )
      )}
    </TransitionGroup>
)

export default Notes;

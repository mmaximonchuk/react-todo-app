import {weatherAPI} from '../api/api';

const TOGGLE_DETAILS_SHOWN = "weather/TOGGLE_DETAILS_SHOWN";
const TOGGLE_CITY_REQUEST_SHOWN = "weather/TOGGLE_CITY_REQUEST_SHOWN";
const SET_WEATHER_DATA = "weather/SET_WEATHER_DATA";
const SET_IS_LODAING = "weather/SET_IS_LODAING";
const SET_CITY_DETAILS = "weather/SET_CITY_DETAILS";
const SET_REQUIRED_CITY_NAME = "weather/SET_REQUIRED_CITY_NAME";
const SET_DETAILS_LOADING = "weather/SET_DETAILS_LOADING";
const ADD_NEW_CITY_TO_REQUEST = "weather/ADD_NEW_CITY_TO_REQUEST";
const SET_CITIES_DATA = "weather/SET_CITIES_DATA";
const REMOVE_CITY = "weather/REMOVE_CITY";
// const SET_ARRAY_OF_CURRENT_CITY_NAMES = "weather/SET_ARRAY_OF_CURRENT_CITY_NAMES";
const SET_ARRAY_OF_CURRENT_CITY_IDS = "weather/SET_ARRAY_OF_CURRENT_CITY_IDS";
const SET_ERROR_MSG = "weather/SET_ERROR_MSG"

let currentAvailableCityId = 1;
const initialState = {
  cities: [],
  // arrayOfCurrentCityNames: [],
  arrayOfCurrentCityIds: [],
  citiesData: [],
  cityDetails: null,
  daysLasted: null,
  isDetailsShown: false,
  isCityRequestShown: false,
  errorMsg: '',
};

const weatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_DETAILS_SHOWN: {
      return {
        ...state,
        isDetailsShown: action.toggleFlag,
      };
    }
    case TOGGLE_CITY_REQUEST_SHOWN: {
      return {
        ...state,
        isCityRequestShown: action.toggleFlag,
      };
    }
    case SET_REQUIRED_CITY_NAME: {
      return {
        ...state,
        cities: action.cityName,
      };
    }
    case SET_IS_LODAING: {
      return {
        ...state,
        isLoading: action.loadingState,
      };
    }
    case SET_DETAILS_LOADING: {
      return {
        ...state,
        isLoading: action.loadingState,
      };
    }
    case SET_WEATHER_DATA: {
      return {
        ...state,
        weatherData: action.weatherData,
      };
    }
    case SET_CITY_DETAILS: {
      let days = action.details.list.map(item => item.dt_txt);


      let formattedData = action.details.list.map(item => {
        let day = new Date(item.dt_txt).toLocaleString('en', {day: '2-digit'})
        for(let i = 0; i < days.length; i++) {
            return {
              ...item,
              day: Number.parseInt(day)
            }
        }
      });
      
      return {
        ...state,
        cityDetails: formattedData,
        daysLasted: days,
      };
    }
    case ADD_NEW_CITY_TO_REQUEST: {
      return {
        ...state,
        cities: [...state.cities, action.newRequestedCityObj],
      };
    }
    case SET_CITIES_DATA: {
      return {
        ...state,
        citiesData: [...state.citiesData, action.newCityData],
      };
    }
    // case SET_ARRAY_OF_CURRENT_CITY_NAMES: {
    //   return {
    //     ...state,
    //     arrayOfCurrentCityNames: [...state.arrayOfCurrentCityNames, action.cityName],
    //   };
    // }
    case SET_ARRAY_OF_CURRENT_CITY_IDS: {
      return {
        ...state,
        arrayOfCurrentCityIds: [...state.arrayOfCurrentCityIds, action.cityId],
      };
    }
    case SET_ERROR_MSG: {
      return {
        ...state,
        errorMsg: action.err,
      };
    }
    case REMOVE_CITY: {
      const newCityArray = JSON.stringify(JSON.parse(localStorage.getItem('cities'), null, 2).filter(c => c !== action.cityName))
      localStorage.setItem('cities', newCityArray)
      return {
        ...state,
        citiesData: state.citiesData.filter(city => city.id !== action.cityId),
        arrayOfCurrentCityNames: state.arrayOfCurrentCityNames.filter(city => city !== action.cityName),
      };
    }
    default:
      return state;
  }
};

export const toggleDetailsShown = (toggleFlag) => {
  return {
    type: TOGGLE_DETAILS_SHOWN,
    toggleFlag,
  };
};
export const toggleCityRequestShown = (toggleFlag) => {
  return {
    type: TOGGLE_CITY_REQUEST_SHOWN,
    toggleFlag,
  };
};
export const setWeatherData = (weatherData) => {
  return {
    type: SET_WEATHER_DATA,
    weatherData,
  };
};
export const setCityDetails = (details) => {
  return {
    type: SET_CITY_DETAILS,
    details,
  };
};
export const setRequiredCityName = (cityName) => {
  return {
    type: SET_REQUIRED_CITY_NAME,
    cityName,
  };
};
export const setIsLoading = (loadingState) => {
  return {
    type: SET_IS_LODAING,
    loadingState,
  };
};
export const setDetailsLoading = (loadingState) => {
  return {
    type: SET_DETAILS_LOADING,
    loadingState,
  };
};
export const addNewCityToRequest = (newRequestedCity) => {
  currentAvailableCityId += 1;
  return {
    type: ADD_NEW_CITY_TO_REQUEST,
    newCityData: { id: currentAvailableCityId, cityName: newRequestedCity},
  };
};
export const setCitiesData = (newCityData) => {
  return {
    type: SET_CITIES_DATA,
    newCityData,
  };
};
export const removeCity = (cityId, cityName) => {

  return {
    type: REMOVE_CITY,
    cityId,
    cityName,
  };
};
// export const setArrayOfCurrentCityNames = (cityName) => {
//   return {
//     type: SET_ARRAY_OF_CURRENT_CITY_NAMES,
//     cityName
//   }
// };
export const setArrayOfCurrentCityIds = (cityId) => {
  return {
    type: SET_ARRAY_OF_CURRENT_CITY_IDS,
    cityId
  }
};
export const setErrMsg = (err) => {
  return {
    type: SET_ERROR_MSG,
    err
  }
}
export const requestCityThunk = (city, arrayOfCurrentCityIds) => async (dispatch) => {
  dispatch(setIsLoading(true));
  const response = await weatherAPI.requestCityWeather(city);


  if(arrayOfCurrentCityIds.includes(response.id)) {
    dispatch(setErrMsg('You already have requested this city! Please, request another one.'))
  } else {
    dispatch(setWeatherData(response));
    dispatch(setArrayOfCurrentCityIds(response.id));
    dispatch(setCitiesData(response));
  }
  dispatch(setIsLoading(false));

};
export const requestCityWeatherWithCoordsThunk = (lat, lon) => async (dispatch) => {
  dispatch(setIsLoading(true));
  const response = await weatherAPI.requestCityWeatherWithCoords(lat, lon);
  dispatch(setWeatherData(response));
  // dispatch(setArrayOfCurrentCityNames(response.name.toLowerCase()))
  dispatch(setArrayOfCurrentCityIds(response.id));
  dispatch(setCitiesData(response));
  dispatch(setIsLoading(false));
};

export const requestCityDetailsThunk = (city) => async (dispatch) => {
  dispatch(setDetailsLoading(true));
  const response = await weatherAPI.requestCityDetails(city);
  // console.log(response);
  dispatch(setCityDetails(response));
  dispatch(setDetailsLoading(false));
};

export default weatherReducer;

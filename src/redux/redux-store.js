import { applyMiddleware, combineReducers, createStore } from "redux";
import shopReducer from './shopReducer';
import thunkMiddleware from 'redux-thunk';
import weatherReducer from "./weatherReducer";

const reducerBatch = combineReducers({
    shopPage: shopReducer,
    weatherForecast: weatherReducer,
});
const store = createStore(reducerBatch, applyMiddleware(thunkMiddleware));

export default store;
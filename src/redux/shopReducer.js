import imgCarrot from "../images/carrot.jfif";
import imgWasher from "../images/washer.jfif";
import imgGun from "../images/gun.jpg";
import imgGarlic from "../images/garlic.jpg";

const ADD_TO_BUSKET = "ADD-TO-BUSKET";
const CULC_TOTAL_SUM = "CULC-TOTAL-SUM";
const SET_PRODUCT_TOTAL_PRICE = "SET-PRODUCT-TOTAL-PRICE";
const REMOVE_FROM_BUSKET = "REMOVE-FROM-BUSKET";
const SET_QUANTITY_OF_GOOD = "SET-QUANTITY-OF-GOOD";

const initialState = {
  goods: [
    {
      id: 1,
      title: "carrot",
      description: "some specific description bla bla...",
      price: 100,
      image: imgCarrot,
      quantity: 1,
    },
    {
      id: 2,
      title: "washer",
      description: "some specific description bla bla...",
      price: 9623,
      quantity: 1,
      image: imgWasher,
    },
    {
      id: 3,
      title: "pistol",
      description: "some specific description bla bla...",
      price: 2320,
      quantity: 1,
      image: imgGun,
    },
    {
      id: 4,
      title: "garlic",
      description: "some specific description bla bla...",
      price: 250,
      quantity: 4,
      image: imgGarlic,
    },
  ],
  basketItems: [],
  culcTotalSum: 0,
  showModalCart: false,
};

const shopReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_BUSKET:{
      if(containsElement(state.basketItems, action.goodId)) {
        return {
          ...state,
          basketItems: state.basketItems.map(item => {
            if(item.id === action.goodId) {
               return {...item, quantity: item.quantity + 1}
            }
            return item
          })
        }
      }
      return {
          ...state,
          basketItems: [...state.basketItems, ...state.goods.filter((item) => item.id === action.goodId)]
      }
      
    }
    case REMOVE_FROM_BUSKET:
      return {
        ...state,
        basketItems: [
          ...state.basketItems.filter((item) => item.id !== action.goodId),
        ],
      };
    case CULC_TOTAL_SUM: {
      let values = state.basketItems.map((item) => item.price * item.quantity);
      const sum = (arr) => {
        let sum = 0;
        if (arr.length) {
          sum = arr.reduce((a, b) => {
            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
          });
        } else {
          sum = 0;
        }
        return sum;
      };
      return {
        ...state,
        culcTotalSum: sum(values),
      };
    }
    case SET_PRODUCT_TOTAL_PRICE: {
      return {
        ...state,
        basketItems: [
          ...state.basketItems,
          ...state.basketItems.map((item) => {
            return {
              ...item,
              totalPrice: item.price * item.quantity,
            };
          }),
        ],
      };
    }
    case SET_QUANTITY_OF_GOOD: {
        return {
          ...state,
          basketItems: state.basketItems.map(item => {
            action.value = action.value < 1 ? 1 : action.value
            if(item.id === action.goodId) {
               return {...item, quantity: action.value} 
            }
            return item
          })
      }
    } 
    default:
      return state;
  }
};

function containsElement(arrOfObj1, goodId) {
    for (let i = 0; i < arrOfObj1.length; i++) {
      if (arrOfObj1[i].id === goodId) {
          return true;
      }
    }
  return false;
}

export const addToBusket = (goodId) => {
  return {
    type: ADD_TO_BUSKET,
    goodId,
  };
};
export const setCulcTotalSum = (arrayOfGoods) => {
  return {
    type: CULC_TOTAL_SUM,
    arrayOfGoods,
  };
};
export const removeGoodFromBasket = (goodId) => {
  return {
    type: REMOVE_FROM_BUSKET,
    goodId,
  };
};
export const setProductTotalPrice = () => {
    return {
      type: SET_PRODUCT_TOTAL_PRICE,
    };
};
export const setQuantityOfGood = (goodId, value) => {
    return {
      type: SET_QUANTITY_OF_GOOD,
      value,
      goodId
    };
};
export default shopReducer;

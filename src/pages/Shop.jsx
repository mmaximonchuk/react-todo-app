import React, { useState, useEffect } from "react";
import Good from "../components/Good";
import Basket from "../components/Basket";
import Modal from "../components/Modal";
import { connect } from "react-redux";

import {
  addToBusket,
  setCulcTotalSum,
  removeGoodFromBasket,
  setProductTotalPrice,
  setQuantityOfGood,
} from "../redux/shopReducer";

const Shop = (props) => {
  return (
    <div className="small-shop">
      <div className="row">
        {props.showModalCart ? (
          <Modal setShowModalCart={props.setShowModalCart}>
            <Basket
              culcTotalSum={props.culcTotalSum}
              setCulcTotalSum={props.setCulcTotalSum}
              setQuantityOfGood={props.setQuantityOfGood}
              removeGoodFromBasket={props.removeGoodFromBasket}
              basketItems={props.basketItems}
              setProductTotalPrice={props.setProductTotalPrice}
            />
          </Modal>
        ) : (
          ""
        )}
        <div className="col-md-8 col-12">
          <div className="cards-container">
            {props.goods.map((good) => {
              return (
                <Good key={good.id} addToBusket={props.addToBusket} {...good} />
              );
            })}
          </div>
        </div>
        <div className="col-md-4 col-12">
          <h3 className="text-center">Shopping Cart</h3>
          <Basket
            culcTotalSum={props.culcTotalSum}
            setCulcTotalSum={props.setCulcTotalSum}
            setQuantityOfGood={props.setQuantityOfGood}
            removeGoodFromBasket={props.removeGoodFromBasket}
            basketItems={props.basketItems}
            setProductTotalPrice={props.setProductTotalPrice}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    goods: state.shopPage.goods,
    basketItems: state.shopPage.basketItems,
    culcTotalSum: state.shopPage.culcTotalSum,
  };
};

const WrappedShop = connect(mapStateToProps, {
  addToBusket,
  setCulcTotalSum,
  removeGoodFromBasket,
  setProductTotalPrice,
  setQuantityOfGood,
})(Shop);

export default WrappedShop;

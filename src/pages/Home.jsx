import React, {useContext, useEffect} from 'react';
import Alert from '../components/Alert';
import Form from '../components/Form';
import Loader from '../components/Loader';
import Notes from '../components/Notes';
import { FirebaseContext } from '../context/firebase/firebaseContext';

const Home = () => {
    const {isLoading, notes, fetchNotes, removeNote} = useContext(FirebaseContext);

    // useEffect(() => {
    //     fetchNotes()
    //     // eslint-disable-next-line
    // }, [])
    return (
        <>
            <Form/>
            <hr/>

            {isLoading 
                ? <Loader /> 
                : <Notes notes={notes} onRemove={removeNote}/>
            }
            
        </>
    );
};

export default Home;
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
// import { TransitionGroup, CSSTransition } from "react-transition-group";
import iconSerach from "../images/search.svg";
import WeatherCityCard from "../components/WeatherCityCard";
import WeatherCityDetails from "../components/WeatherCityDetails";

import WeatherLoader from "../components/WeatherLoader";
import {
  toggleDetailsShown,
  // setWeatherData,
  // setIsLoading,
  requestCityThunk,
  requestCityDetailsThunk,
  setRequiredCityName,
  addNewCityToRequest,
  removeCity,
  toggleCityRequestShown,
  requestCityWeatherWithCoordsThunk,
  // setArrayOfCurrentCityNames,
  setArrayOfCurrentCityIds,
  // setCitiesData,
} from "../redux/weatherReducer";
import WeatherCityRequest from "../components/WeatherCityRequest";
import WeatherModal from "../utils/WeatherModal";

const Forecast = ({
  isDetailsShown,
  toggleDetailsShown,
  requestCityThunk,
  requestCityDetailsThunk,
  cityDetails,
  cities,
  addNewCityToRequest,
  citiesData,
  removeCity,
  daysLasted,
  toggleCityRequestShown,
  isCityRequestShown,
  requestCityWeatherWithCoordsThunk,
  // setArrayOfCurrentCityNames,
  // arrayOfCurrentCityNames,
  setArrayOfCurrentCityIds,
  arrayOfCurrentCityIds,
  errorMsg,
}) => {
  const [currentCityToRender, setCurrentCityToRender] = useState("");
  const [isOpenMobileMenu, setStateMobileMenu] = useState(false);

  const handleStateMobileMenu = (flag) => {
    setStateMobileMenu(flag);

    const body = document.querySelector('body');
    if (isOpenMobileMenu) {
      body.style.overflow = 'auto';
    } else {
      body.style.overflow = 'hidden';
    }
  };

  const scrollToTop = (e) => e.target.onclick = () => window.scrollTo(0, 0)
  

  useEffect(() => {
    if (!isOpenMobileMenu) {
      const body = document.querySelector('body');
      body.style.overflow = 'auto';
    }
  }, [isOpenMobileMenu]);

  const requestDetails = (cityName) => {
    requestCityDetailsThunk(cityName);
    setCurrentCityToRender(cityName);
  };

  const requestNewCity = (cityName) => {
    addNewCityToRequest(cityName);
    requestCityThunk(cityName, arrayOfCurrentCityIds);
    
    if(localStorage.getItem('cities')?.length) {
      const currentCitiesArray = JSON.parse(localStorage.getItem('cities'), null, 2)
      localStorage.setItem('cities', JSON.stringify([...currentCitiesArray, cityName]))
    } else {
      localStorage.setItem('cities', JSON.stringify([cityName]))
    }

    if (errorMsg) {
      console.log(errorMsg);
    }
  };

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const lat = position.coords.latitude;
      const lon = position.coords.longitude;
      requestCityWeatherWithCoordsThunk(lat, lon);
    });
    const currentCitiesArray = JSON.parse(localStorage.getItem('cities'), null, 2)

    if(currentCitiesArray?.length) {
      currentCitiesArray.forEach(city => requestCityThunk(city, arrayOfCurrentCityIds))
    }

    cities.forEach((city) => {
      console.log(city);
      requestCityThunk(city.cityName, arrayOfCurrentCityIds);
          setArrayOfCurrentCityIds(city.id);
    });
  }, []);


  // console.log(arrayOfCurrentCityIds);
  return (
    <main className="forecast-section">
      <section className="cities-container">
      
        {!citiesData ? (
          <WeatherLoader className="main-loader" />
        ) : (
          <>
            {citiesData.map((city, index) => {
              return (
                <WeatherCityCard
                  id={city.id}
                  key={city.id}
                  weatherData={city}
                  toggleDetailsShown={toggleDetailsShown}
                  requestDetails={requestDetails}
                  removeCity={removeCity}
                  // arrayOfCurrentCityNames={arrayOfCurrentCityNames}
                  arrayOfCurrentCityIds={arrayOfCurrentCityIds}
                />
              );
            })}
          </>
        )}

        {isDetailsShown && (
          <WeatherModal toggleFunction={toggleDetailsShown}>
            <WeatherCityDetails
              daysLasted={daysLasted}
              cityDetails={cityDetails}
              city={currentCityToRender}
            />
          </WeatherModal>
        )}
        {isCityRequestShown && (
          <WeatherModal toggleFunction={toggleCityRequestShown} handleStateMobileMenu={handleStateMobileMenu}>
            <WeatherCityRequest
              requestNewCity={requestNewCity}
              toggleCityRequestShown={toggleCityRequestShown}
              // arrayOfCurrentCityNames={arrayOfCurrentCityNames}
              handleStateMobileMenu={handleStateMobileMenu}
            />
          </WeatherModal>
        )}
      </section>
      <button
        onClick={(e) => {
          toggleCityRequestShown(!isCityRequestShown)
          handleStateMobileMenu(true);
          scrollToTop(e);
        }}
        className="search-btn"
      >
        <img src={iconSerach} alt="" />
      </button>
    </main>
  );
};

const mapStateToProps = (state) => ({
  isDetailsShown: state.weatherForecast.isDetailsShown,
  isCityRequestShown: state.weatherForecast.isCityRequestShown,
  isLoading: state.weatherForecast.isLoading,
  weatherData: state.weatherForecast.weatherData,
  cityDetails: state.weatherForecast.cityDetails,
  cities: state.weatherForecast.cities,
  detailsLoading: state.weatherForecast.detailsLoading,
  citiesData: state.weatherForecast.citiesData,
  daysLasted: state.weatherForecast.daysLasted,
  // arrayOfCurrentCityNames: state.weatherForecast.arrayOfCurrentCityNames,
  arrayOfCurrentCityIds: state.weatherForecast.arrayOfCurrentCityIds,
  errorMsg: state.weatherForecast.errorMsg,
});

export default connect(mapStateToProps, {
  toggleDetailsShown,
  setRequiredCityName,
  requestCityThunk,
  requestCityDetailsThunk,
  addNewCityToRequest,
  removeCity,
  toggleCityRequestShown,
  requestCityWeatherWithCoordsThunk,
  setArrayOfCurrentCityIds,
  // setCitiesData,
  // setArrayOfCurrentCityNames,
})(Forecast);

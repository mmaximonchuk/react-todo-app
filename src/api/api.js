import * as axios from "axios";

// process.env.WEATHER_API_KEY
const apiKey = '494eb3412df62f843cf6aa95234e427f';

export const instance = axios.create({
    baseURL: `https://api.openweathermap.org/data/2.5`,
})

export const weatherAPI = {
    requestCityWeather: async (city) => {
        const response = await instance.get(`weather?q=${city}&units=metric&lang=en&appid=${apiKey}`)             
        return await response.data
    },
    requestCityWeatherWithCoords: async (lat, lon) => {
        const response = await instance.get(`weather?lat=${lat}&lon=${lon}&units=metric&lang=en&appid=${apiKey}`)             
        return await response.data
    },
    requestCityDetails: async (city) => {
        const response = await instance.get(`forecast/?q=${city}&units=metric&lang=en&cnt=96&appid=${apiKey}`)             
        return await response.data
    },
}
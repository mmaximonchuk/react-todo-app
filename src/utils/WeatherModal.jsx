import React from 'react';

const WeatherModal = ({children, toggleFunction, handleStateMobileMenu = false}) => {
    return (
        <div className="city-modal">
                {children}
                <button onClick={() => {
                    toggleFunction(false)
                    if(handleStateMobileMenu) {
                        handleStateMobileMenu(false);
                    }
                }} className="close-btn">
                    &times;
                </button>
        </div>
    );
};

export default WeatherModal;
import React, { useState, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Alert from "./components/Alert";
import Header from "./components/Header";

import Shop from "./pages/Shop";
// import Home from "./pages/Home";
// import Modal from './components/Modal';
import Forecast from "./pages/Forecast";

const App = () => {
  const [showModalCart, setShowModalCart] = useState(false);

  return (
    
        <BrowserRouter>
          <Header setShowModalCart={setShowModalCart}/>
          <div className="container pt-4">
            <Alert />
            <Switch>
              {/* <Route path={"/"} exact component={Home} /> */}
              {/* <Route path={"/shop"} exact component={() => <Shop setShowModalCart={setShowModalCart} showModalCart={showModalCart}/> } /> */}
              <Route path={"/"} exact component={() => <Forecast /> } />

            </Switch>
          </div>
        </BrowserRouter>
  );
};

export default App;
